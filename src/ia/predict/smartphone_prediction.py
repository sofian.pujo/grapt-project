# Disable warnings
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import numpy as np
import tensorflow as tf
import sys

from tensorflow import keras

img_height = 512
img_width = 512
class_names = ['broken', 'fine']

# Chargement du modèle
model_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'smartphone_model.h5')
model = tf.keras.models.load_model(model_path)

# Chargement et traitement de l'image
sunflower_path = sys.argv[1]
img = keras.preprocessing.image.load_img(
    sunflower_path, target_size=(img_height, img_width)
)
img_array = keras.preprocessing.image.img_to_array(img)
img_array = tf.expand_dims(img_array, 0)

# Prediction
predictions = model.predict(img_array)
score = tf.nn.softmax(predictions[0])

# Sortie
print(class_names[np.argmax(score)])
print(np.max(score))
