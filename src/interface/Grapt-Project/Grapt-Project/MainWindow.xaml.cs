﻿using System;
using System.Windows;
using System.Windows.Controls;
using Grapt_Project.Class;
using System.IO;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Generic;

namespace Grapt_Project
{ 
    public partial class MainWindow : Window
    {
        public Configuration Config { get; private set; }
        private ImgDetails imgDetails;
        private Details imgData;
        private JsonDetails jsonDetails;
        public MainWindow()
        {
            InitializeComponent();
            Config = new Configuration();
            imgDetails = new ImgDetails();
            gridStats.Visibility = Visibility.Hidden;
            gridPiece.Visibility = Visibility.Hidden;
            loadTreeView();
            jsonDetails = new JsonDetails();
            imgData = jsonDetails.GetData();
        }

        private void buttonConfiguration_Click(object sender, RoutedEventArgs e)
        {
            Interfaces.Configuration configWindow = new Interfaces.Configuration();
            configWindow.Show();
        }

        private void loadTreeView()
        {
            TreeView tree = treeViewFolderImg;
            TreeViewItem yearTreeView = new TreeViewItem();
            TreeViewItem monthTreeView = new TreeViewItem();
            TreeViewItem dayTreeView = new TreeViewItem();
            string yearPath = "";
            string monthPath = "";
            string dayPath = "";

            tree.Items.Clear();
            imgDetails = new ImgDetails();

            foreach (string folder in imgDetails.ImgDict.Keys)
            {
                yearTreeView.Header = folder;
                yearTreeView.Tag = "y";
                yearPath = Config.ImgFolder + @"\" + folder;
                // Ajout des années
                foreach (string folderMonth in Directory.EnumerateDirectories(yearPath))
                {
                    monthTreeView.Header = folderMonth.Substring(folderMonth.LastIndexOf("\\") + 1);
                    monthTreeView.Tag = "m";
                    monthPath = yearPath + @"\" + folderMonth.Substring(folderMonth.LastIndexOf("\\") + 1);
                        foreach (string folderDay in Directory.EnumerateDirectories(monthPath))
                        {
                            dayTreeView.Header = folderDay.Substring(folderDay.LastIndexOf("\\") + 1);
                            dayTreeView.Tag = "d";
                            dayPath = monthPath + @"\" + folderDay.Substring(folderDay.LastIndexOf("\\") + 1);
                            foreach (string img in Directory.EnumerateFiles(dayPath))
                            {
                                dayTreeView.Items.Add(img.Substring(img.LastIndexOf("\\") + 1));
                            }
                            monthTreeView.Items.Add(dayTreeView);
                            dayTreeView = new TreeViewItem();
                    }
                    yearTreeView.Items.Add(monthTreeView);
                    monthTreeView = new TreeViewItem();
                }
                tree.Items.Add(yearTreeView);
                yearTreeView = new TreeViewItem();
            }
        }

        private void treeViewFolderImg_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem selectedNode = null;
            gridStats.Visibility = Visibility.Hidden;
            gridPiece.Visibility = Visibility.Hidden;

            if (treeViewFolderImg.SelectedItem is TreeViewItem)
            {
                selectedNode = (TreeViewItem)treeViewFolderImg.SelectedItem;
            }
            
            if (selectedNode != null && selectedNode.Tag.ToString() == "m")
            {
                labelTitleStats.Content = "Statistiques du mois";
                getMonthStat(selectedNode.Header.ToString().ToString());
                gridStats.Visibility = Visibility.Visible;
            }
            else if (selectedNode == null && treeViewFolderImg.SelectedItem != null)
            {
                updateUiSingleImage();
            }
            else
            {
                gridStats.Visibility = Visibility.Hidden;
            }
        }

        private void updateUiSingleImage()
        {
            ImgData img = imgData.getSingleImgDetails(Int32.Parse(treeViewFolderImg.SelectedItem.ToString().Split('.')[0]));
            labelPieceNumeroEdit.Content = img.id;
            labelPieceDefectEdit.Content = 1 - img.accuracy;
            imgPiece.Source = new BitmapImage(new Uri(Config.ImgFolder + "/" + img.date.Split('/')[2] + "/" + img.date.Split('/')[1] + "/" + img.date.Split('/')[0] + "/" + treeViewFolderImg.SelectedItem));
            SolidColorBrush brush = new SolidColorBrush();
            if (img.accuracy >= 0.96)
            {
                brush.Color = Color.FromArgb(255, 18, 201, 0);
            }
            else
            {
                brush.Color = Color.FromArgb(255, 193, 0, 0);
            }
            indicatorStatePiece.Fill = brush;
            gridPiece.Visibility = Visibility.Visible;
        }

        private void getMonthStat(string month)
        {
            List<ImgData> listImg = new List<ImgData>();
            listImg = imgData.getMonthImgDetails(month);
            int nbScannedFailed = 0;
            double totalAccuracy = 0;
            double totalAccuracyFailed = 0;
            double percentFailed = 0;
            int nbScanned = listImg.Count;

            foreach(ImgData img in listImg)
            {
                totalAccuracy += img.accuracy;
                if (img.accuracy < 0.90)
                {
                    nbScannedFailed += 1;
                    totalAccuracyFailed += img.accuracy;
                }
            }
            percentFailed = nbScannedFailed;
            percentFailed /= nbScanned;
            totalAccuracy /= nbScanned;
            totalAccuracyFailed /= nbScannedFailed;

            labelNbScannedNumber.Content = nbScanned;
            labelNbFailedNumber.Content = nbScannedFailed;
            labelPercentFailedNumber.Content = (percentFailed * 100).ToString() + " %";
            labelComparePercentNumber.Content = ((percentFailed - 0.04) * 100).ToString() + " %";
            labelTotalAccuracynumber.Content = (totalAccuracy * 100).ToString() + " %";
            labelTotalAccuracyFailedNumber.Content = (totalAccuracyFailed * 100).ToString() + " %";
        }

        private void panelAnalyse_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SolidColorBrush myBrush = new SolidColorBrush();
            myBrush.Color = Color.FromArgb(255, 230, 230, 230);
            panelAnalyse.Background = myBrush;
        }

        private void panelAnalyse_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SolidColorBrush myBrush = new SolidColorBrush();
            myBrush.Color = Color.FromArgb(255, 255, 255, 255);
            panelAnalyse.Background = myBrush;
        }

        private void panelAnalyse_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Config = new Configuration();
            System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
            SolidColorBrush myBrush = new SolidColorBrush();
            myBrush.Color = Color.FromArgb(255, 230, 230, 230);
            panelAnalyse.Background = myBrush;

            fileDialog.InitialDirectory = "C://";
            fileDialog.Filter = "Images|*.png;*.jpg;*.jpeg";

            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DateTime localDate = DateTime.Now;
                string extension = fileDialog.FileName.Substring(fileDialog.FileName.LastIndexOf("."));
                int index = imgData.getLastIndexOfDay(localDate.Day.ToString());
                if (!Directory.Exists(Config.ImgFolder + "/" + localDate.Year))
                {
                    Directory.CreateDirectory(Config.ImgFolder + "/" + localDate.Year);
                }

                if (!Directory.Exists(Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month))
                {
                    Directory.CreateDirectory(Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month);
                }

                if (!Directory.Exists(Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month + "/" + localDate.Day))
                {
                    Directory.CreateDirectory(Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month + "/" + localDate.Day);
                }

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = Config.pythonPath;
                startInfo.Arguments = "\"" + Directory.GetCurrentDirectory() + "/ressources/smartphone_prediction.py\" \"" + fileDialog.FileName + "\"";
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;
                process.StartInfo = startInfo;
                if (File.Exists(Config.pythonPath))
                {
                    string errorOutput = "";
                    try
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        process.Start();
                        string output = process.StandardOutput.ReadToEnd();
                        errorOutput = process.StandardError.ReadToEnd();
                        process.WaitForExit();
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                        string test = output.Split('\n')[1].Replace("\r", "");
                        double accuracy = Convert.ToDouble(test, System.Globalization.CultureInfo.InvariantCulture);
                        ImgData newImg = new ImgData(localDate.Date.ToString("dd/M/yyyy"), index, accuracy);
                        jsonDetails.addImg(newImg);
                        File.Copy(fileDialog.FileName, Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month + "/" + localDate.Day + "/" + index.ToString() + extension);
                        while (!File.Exists(Config.ImgFolder + "/" + localDate.Year + "/" + localDate.Month + "/" + localDate.Day + "/" + index.ToString() + extension)) { }
                        loadTreeView();
                    } catch (Exception ex)
                    {
                        MessageBox.Show("Une erreur est intervenue : " + errorOutput);
                    }
                } 
                else
                {
                    MessageBox.Show("Le chemin de python n'a pas été trouvé. Il peut être modifié depuis la configuration.");
                }
            }

        }

        private void panelAnalyse_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            SolidColorBrush myBrush = new SolidColorBrush();
            myBrush.Color = Color.FromArgb(255, 200, 200, 200);
            panelAnalyse.Background = myBrush;
        }
    }
}
