﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using Grapt_Project.Class;

namespace Grapt_Project.Interfaces
{
    /// <summary>
    /// Logique d'interaction pour Configuration.xaml
    /// </summary>
    public partial class Configuration : Window
    {
        private Class.Configuration config;
        public Configuration()
        {
            InitializeComponent();
            config = new Class.Configuration();
            textboxImgFolder.Text = config.ImgFolder;
            textboxPython.Text = config.pythonPath;
        }

        private void buttonImgFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog imgFolderDialog = new FolderBrowserDialog();

            if (textboxImgFolder.Text != "")
            {
                if (Directory.Exists(textboxImgFolder.Text))
                {
                    imgFolderDialog.SelectedPath = textboxImgFolder.Text;
                } 
                else
                {
                    imgFolderDialog.SelectedPath = "C://";
                }
            }
            
            if (imgFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textboxImgFolder.Text = imgFolderDialog.SelectedPath + "\\img";
            }
        }

        private void setImgFolderPath(string imgFolder)
        {
            if (!Directory.Exists(imgFolder))
            {
                Directory.CreateDirectory(imgFolder);
            }
            config.setImgFolderPath(imgFolder);
        }

        private void setPythonPath(string pythonPath)
        {
            config.setPythonPath(pythonPath);
        }

        private void buttonValidConfig_Click(object sender, RoutedEventArgs e)
        {
            setImgFolderPath(textboxImgFolder.Text);
            setPythonPath(textboxPython.Text);
            Close();
        }

        private void buttonPythonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog pythonPathDialog = new OpenFileDialog();
            pythonPathDialog.Filter = "Python|python.exe";
            pythonPathDialog.Multiselect = false;

                if (pythonPathDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    textboxPython.Text = pythonPathDialog.FileName;
                }
        }
    }
}
