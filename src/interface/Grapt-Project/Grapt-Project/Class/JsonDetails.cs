﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.IO;

namespace Grapt_Project.Class
{
    public class Details
    {
        public List<ImgData> details { get; set; }

        public ImgData getSingleImgDetails(int id)
        {
            foreach (ImgData img in details)
            {
                if (img.id == id)
                {
                    return img;
                }
            }

            return null;
        }

        public List<ImgData> getMonthImgDetails(string month)
        {
            List<ImgData> listImg = new List<ImgData>();
            foreach (ImgData img in details)
            {
                if (img.date.Split('/')[1] == month)
                {
                    listImg.Add(img);
                }
            }

            return listImg;
        }

        public int getLastIndexOfDay(string day)
        {
            int index = 1;
            int iDay = Int32.Parse(day);

            foreach (ImgData img in details)
            {
                if (img.date.Split('/')[0] == day)
                {
                    if (img.id >= index)
                    {
                        index = img.id;
                    }
                }
            }

            index += 1;

            return index;
        }
    }

    public class ImgData
    {
        public ImgData(string date, int id, double accuracy) 
        {
            this.date = date;
            this.id = id;
            this.accuracy = accuracy;
        }
        public string date { get; set; }
        public int id { get; set; }
        public double accuracy { get; set; }
    }
    class JsonDetails
    {
        private string jsonPath;
        private string jsonString;
        private readonly Details data;

        public JsonDetails()
        {
            jsonPath = "temp/data.json";
            if (File.Exists(jsonPath))
            {
                jsonString = File.ReadAllText(jsonPath);
                data = JsonSerializer.Deserialize<Details>(jsonString);
            }
            else
            {
                if(!Directory.Exists("temp"))
                {
                    Directory.CreateDirectory("temp");
                }
                File.WriteAllText("temp/data.json", "{\"details\":[]}");
            }
        }

        public Details GetData()
        {
            return data;
        }

        public void addImg(ImgData img)
        {
            data.details.Add(img);
            string jsonToWrite = JsonSerializer.Serialize<Details>(data);
            File.WriteAllText("temp/data.json", jsonToWrite);
        }

    }
}
