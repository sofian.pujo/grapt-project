﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Grapt_Project.Class
{
    class ImgDetails
    {
        private Class.Configuration Config;
        public Dictionary<string, List<string>> ImgDict { get; private set; }
        public ImgDetails()
        {
            Config = new Configuration();
            getAllImgInFolders(Config.ImgFolder);
        }

        /**
         * Rempli le dictionnaire des dossiers et images
         * */
        private void getAllImgInFolders(string imgFolder)
        {
            ImgDict = new Dictionary<string, List<string>>();

            foreach (string fullPathFolder in Directory.EnumerateDirectories(imgFolder))
            {
                string folder = fullPathFolder.Split('\\').Last<string>();
                if (!ImgDict.ContainsKey(folder))
                {
                    ImgDict.Add(folder, new List<string>());
                    foreach (string file in Directory.EnumerateFiles(imgFolder + "/" + folder ))
                    {
                        ImgDict[folder].Add(file);
                    }
                }
            }
        }
    }
}
