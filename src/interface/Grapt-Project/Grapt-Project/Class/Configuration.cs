﻿using System.Xml;
using System.IO;

namespace Grapt_Project.Class
{
    public class Configuration
    {
        private XmlDocument configFile;
        public string ImgFolder { get; set; }
        public string pythonPath { get; set; }
        public Configuration()
        {
            configFile = new XmlDocument();
            configFile.Load("Config/Configuration.xml");
            ImgFolder = configFile.SelectSingleNode("grapt/imgFolder").InnerText;
            pythonPath = configFile.SelectSingleNode("grapt/pythonPath").InnerText;
            if (!File.Exists(ImgFolder))
            {
                if (!Directory.Exists("temp/img"))
                {
                    Directory.CreateDirectory("temp/img");
                }
                setImgFolderPath(Path.GetFullPath("temp/img"));
                ImgFolder = Path.GetFullPath("temp/img");
            }
        }

        public void setImgFolderPath(string imgFolder)
        {
            this.ImgFolder = imgFolder;
            configFile.SelectSingleNode("grapt/imgFolder").InnerText = imgFolder;
            configFile.Save("Config/Configuration.xml");
        }

        public void setPythonPath(string pythonPath)
        {
            this.pythonPath = pythonPath;
            configFile.SelectSingleNode("grapt/pythonPath").InnerText = pythonPath;
            configFile.Save("Config/Configuration.xml");
        } 
    } 
}
