# IA de détection de défauts

Cette IA est entrainée à détecter les smartphones ayant subi des dégâts, le but est de fournir un exemple applicable à la détection de pièces industrielles ayant subi un défaut.
L'IA se décompose en deux parties, la partie entrainement et la partie prédictions.

## Technologies

* Tensorflow Keras 2.3.1
* Python 3.7.6

## Entrainement

La phase d'apprentissage va permettre à l'algorithme d'apprendre d'un jeu de données.

* Script d'entrainement : *ia/train/smartphone_train.py*
* Jeu de données : *ia/train/smartphone_dataset.tar.gz*

### Prétraitement des données

Dans un premier temps, les images sont redimensionnées (variables *img_width* et *img_width*).
Le jeu de données est séparé en deux, une partie entrainement (train_ds) et une partie qui sert à la vérification (val_ds).

L'algorithme classe les images dans deux catégories "fine" et "broken".

### Augmentation des données

Une augmentation des données est effectuée sur le jeu d'entrainement, cela permet d'améliorer l'apprentissage et réduire le surapprentissage.

### Construction du modèle

La première couche du modèle applique l'augmentation des données, la seconde applique le redimensionnement.

Ensuite s'enchaînent plusieurs couches de convolution avec une fonction d'activation "relu".

L'une des dernières couches est une couche de dropout, elle supprime aléatoirement un certain nombre de sorties ce qui permet de limiter le suraprentissage.

### Entraînement du modèle

L'entraînement est effectué sur un certain nombre d'epochs, si le nombre est trop faible l'algorithme n'apprend pas assez, s'il est trop fort il y a risque de suraprentissage. Cette étape est la plus coûteuse en ressources.

### Export du modèle

Le modèle est ensuite exporté au format h5 selon la norme HDF5.

## Reconnaissance

### Import du modèle

La reconnaissance utilise le modèle créé lors de la phase d'apprentissage. Il est stocké dans un fichier h5.

### Prédiction

La prédiction se réalise et fournit des niveaux de crédence sur la catégorie "broken" et "file".

### Sortie des données

En sortie, la catégorie est envoyée sur la première ligne et le niveau de crédence sur la seconde.
